<?php

namespace App\Controller\Admin;

use App\Entity\Artiste;
use App\Entity\Concert;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la Sirène');
    }

    public function configureMenuItems(): iterable
    {
        return[
            MenuItem::linkToRoute('Retour au site', 'fas fa-home', 'homepage'),
            MenuItem::linkToCrud('Concerts', 'fa-solid fa-volume-high', Concert::class)
                ->setController(ConcertCrudController::class),
            MenuItem::linkToCrud('Concerts passés', 'fa-solid fa-volume-low', Concert::class)
                ->setController(ConcertPassesCrudController::class),
            MenuItem::linkToCrud('Artistes', 'fas fa-user', Artiste::class),
            MenuItem::linkToCrud('Newsletter', 'fas fa-paper-plane', Email::class),
        ];
    }
}
